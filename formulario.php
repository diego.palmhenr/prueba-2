<!doctype html>
<html lang="es-ES">
    <head>   
        <meta charset="utf-8">
        <!--Etiqueta para el diseño responsivo-->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!--CSS de Bootstrap y personalizado-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="css/estilos.css" rel="stylesheet" type="text/css"/>
        <!--Titulo de la ventana-->
        <title>Registro de usuario</title>
    </head>
    <body>
        <header class="container">
            <!--Jumbotron-->
            <div class="jumbotron jumbotron">
                <div class="container">
                    <h1 class="display-4">Registro de Usuario</h1>
                </div>
            </div>
        </header>
        <section class="container mt-5">
            <form action="index.html" method="POST">
                <div class="form-group row justify-content-center align-items-center">
                    <div class="col-2">
                        <label>Rut</label>
                    </div>
                    <div class="col-4">
                        <input type="text" id="rut" name="rut" class="form-control" maxlength="12" placeholder="11.222.333-4" oninput="validarRut(this)" required>
                    </div>
                </div>
                <div class="form-group row justify-content-center align-items-center">
                    <div class="col-2">
                        <label>Nombres</label>
                    </div>
                    <div class="col-4">
                        <input type="text" name="nombres" class="form-control" placeholder="Ingrese sus nombres" minlength="3" maxlength="100">
                    </div>
                </div>
                <div class="form-group row justify-content-center align-items-center">
                    <div class="col-2">
                        <label>Apellidos</label>
                    </div>
                    <div class="col-4">
                        <input type="text" name = "apellidos"class="form-control" placeholder="Ingrese sus apellidos" minlength="3" maxlength="100">
                    </div>
                </div>
                <div class="form-group row justify-content-center align-items-center">
                    <div class="col-2">
                        <label>Fecha de nacimiento</label>
                    </div>
                    <div class="col-4">
                        <input type="date" name="fecha" class="form-control">
                    </div>
                </div>
                <div class="form-group row justify-content-center align-items-center">
                    <div class="col-2">
                        <label>Edad</label>
                    </div>
                    <div class="col-4">
                        <input type="text" name="edad" class="form-control">
                    </div>
                </div>
                <div class="form-group row justify-content-center align-items-center">
                    <div class="col-2">
                        <label>Correo Electrónico</label>
                    </div>
                    <div class="col-4">
                        <input type="email" name="email" class="form-control">
                    </div>
                </div>
                <div class="form-group row justify-content-center align-items-center">
                    <div class="col-2">
                        <label>Región</label>
                    </div>
                    <div class="col-4">
                        <select class="custom-select" name="region" id="region">
                            <option selected value="0">-- Seleccione --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row justify-content-center align-items-center">
                    <div class="col-2">
                        <label>Provincia</label>
                    </div>
                    <div class="col-4">
                        <select class="custom-select" name="provincia" id="provincia">
                            <option selected value="0">-- Seleccione --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row justify-content-center align-items-center">
                    <div class="col-2">
                        <label>Comuna</label>
                    </div>
                    <div class="col-4">
                        <select class="custom-select" name="comuna" id="comuna">
                            <option selected value="0">-- Seleccione --</option>
                        </select>
                    </div>
                </div>
                <!-- Button trigger modal -->
                <div class="text-center">
                    <button type="submit" id="prueba" class="btn btn-info" data-toggle="modal" data-target="#exampleModal" >
                        Crear Usuario
                    </button>
                </div>
            </form>
        </section>
        <footer class="container mt-5 bg-secondary">
            <div class="py-3 text-center">
                <h2 class="display-5">Carrera Analista Programador Computacional</h2>
                <p>Integrantes: Sebastián Labrín Urrutia - Diego Palma Henríquez - Álvaro Gutiérrez Burdiles</p>
                <p class="display-4">DUOC UC</p>
            </div>
        </footer>
        <!--Archivos JavaScript de Jquery, Pooper, Bootstrap y personalizado -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="js/localidades.js" type="text/javascript"></script>
        <script src="js/main.js" type="text/javascript"></script>
    </body>
</html>
